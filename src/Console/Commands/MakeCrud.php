<?php

namespace Jigardarji\LaravelCrud\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class MakeCrud extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:crud_laravel {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate Crud Operation';
    protected $softDelete = true;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $this->name = $this->argument('name');
        $this->fs = new Filesystem();

        $this->controllerGenerate($this->name);
        $this->viewGenerate($this->name);

        $this->line("");
        $this->info("Crud Generated successfully.");
    }

    protected function getStubContent($path)
    {
        if ($this->fs->exists(resource_path('crud-stubs/' .  $path . '.stub'))) {
            return $this->fs->get(resource_path('crud-stubs/' .  $path . '.stub'));
        } else {
            return $this->fs->get(__DIR__ . '/../../stubs/' . $path . '.stub');
        }
    }

    public function controllerFile()
    {
        return $this->getStubContent("NameHereController.php");
    }

    public function indexViewFile()
    {
        return $this->getStubContent("index.blade.php.stub");
    }

    public function createViewFile()
    {
        return $this->getStubContent("create.blade.php.stub");
    }

    public function editViewFile()
    {
        return $this->getStubContent("edit.blade.php.stub");
    }

    public function controllerGenerate($name)
    {
        $pathController = $this->generateFile('app/Http/Controllers/'. $name . 'Controller.php', $this->controllerFile(),'app');
        $this->replaceTextFileAgain($pathController, lcfirst($name));
        $this->replaceTextFile($pathController, $name);

        $this->line("Created File: " . 'app/Http/Controllers/'. $name . 'Controller.php');
        return true;
    }

    public function viewGenerate($name)
    {
        $pathIndex = $this->generateFile('resources/views/'. $name . 'index.blade.php', $this->indexViewFile(),'resources');
        $pathCreate = $this->generateFile('resources/views/'. $name . 'create.blade.php', $this->createViewFile(),'resources');
        $pathEdit = $this->generateFile('resources/views/'. $name . 'edit.blade.php', $this->editViewFile(),'resources');
        $this->replaceTextFileAgain($pathIndex, lcfirst($name));
        $this->replaceTextFile($pathIndex, $name);
               
        $this->replaceTextFileAgain($pathCreate, lcfirst($name));
        $this->replaceTextFile($pathCreate, $name);

        $this->replaceTextFileAgain($pathEdit, lcfirst($name));
        $this->replaceTextFile($pathEdit, $name);

        $this->line("Created File: " . 'resources/views/'. $name . 'index.blade.php');
        $this->line("Created File: " . 'resources/views/'. $name . 'create.blade.php');
        $this->line("Created File: " . 'resources/views/'. $name . 'edit.blade.php');
        return true;
    }

    public function generateFile($path, $file,$type)
    {
        if($type == 'app'){
            $path = app_path($path);
        } else {
            $path = resource_path($path);
        }
        $fh = fopen($path, 'w') or die("can't open file");
        $stringData = $file;
        fwrite($fh, $stringData);
        fclose($fh);
        return $path;
    }

    public function replaceTextFile($path, $name)
    {
        $str = file_get_contents($path);
        $str = str_replace('NameHere', $name, $str);
        file_put_contents($path, $str);
    }

    public function replaceTextFileAgain($path, $name)
    {
        $str = file_get_contents($path);
        $str = str_replace('smallNameHere', $name, $str);
        file_put_contents($path, $str);
    }
}