<?php

namespace Jigardarji\LaravelRepoPattern;

use Illuminate\Support\ServiceProvider;

class LaravelCrudGeneratorServiceProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->commands(\Jigardarji\LaravelCrud\Console\Commands\MakeCrud::class);
    }

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/stubs' => resource_path('crud-stubs')
        ]);
    }
}